package src;

public class Shop {
    private double totalPrice;
    private double appleKg;
    private double peachKg;
    private double melonKg;
    private boolean bag;
    private String customerId;
    private double peachWeight;

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getAppleKg() {
        return appleKg;
    }

    public void setAppleKg(double appleKg) {
        this.appleKg = appleKg;
    }

    public double getPeachKg() {
        return peachKg;
    }

    public void setPeachKg(double peachKg) {
        this.peachKg = peachKg;
    }

    public double getMelonKg() {
        return melonKg;
    }

    public void setMelonKg(double melonKg) {
        this.melonKg = melonKg;
    }

    public boolean isBag() { // на boolean тип нужно вместо "get" "is" писать?
        return bag;
    }

    public void setBag(boolean bag) {
        this.bag = bag;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }



    public Shop() {
    }

    public Shop(double totalPrice, boolean bag, double appleKg, String customerId) {
        this.totalPrice = totalPrice;
        this.appleKg = appleKg;
        this.customerId = customerId;
        this.bag = bag;
    }

    public Shop(double totalPrice, double peachKg, boolean bag, String customerId) {
        this.totalPrice = totalPrice;
        this.peachKg = peachKg;
        this.bag = bag;
        this.customerId = customerId;

    }

    public Shop(double totalPrice, double melonKg, String customerId, boolean bag) {
        this.bag = bag;
        this.customerId = customerId;
        this.melonKg = melonKg;
        this.totalPrice = totalPrice;
    }

    void peachPurchase(double peachWeight){
        if (bag == true){
            totalPrice = peachWeight * peachKg + 1; // 1 means bag price
        }
        else {
            totalPrice = peachWeight * peachKg;
        }
    }

}
